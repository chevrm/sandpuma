#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## Get pairwise PIDs for each A-domain
system("diamond makedb --in full.adom.faa -d full.adom") unless(-e 'full.adom.dmnd');
system("diamond blastp -q full.adom.faa -d full.adom -e 1 -f tab -o full.adom.dbp -p 10 -k 1000000") unless(-e 'full.adom.dbp');
open my $dfh, '<', 'full.adom.dbp' or die $!;
my %fid = ();
while(<$dfh>){
    chomp;
    my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
    $fid{$query}{$hit} = $pctid/100;
}
close $dfh;

## Read in the spec data
my %sp = ();
open my $sfh, '<', 'corenrps.specs.txt' or die $!;
while(<$sfh>){
    chomp;
    my($single, $verbose) = split(/\t/, $_);
    $sp{$verbose} = $single;
}
close $sfh;

## Make the clade config file
open my $ofh, '>', 'corenrps.clade.txt' or die $!;
my $index = 1;
print $ofh join("\t", 'index', 'clade_index', 'PhyloNode_index', 'function_annotation', 'clade_index_ori')."\n";
foreach my $spec (sort keys %sp){
    print $ofh join("\t", $index, $sp{$spec}, $index, $sp{$spec}, 'index'.$index)."\n";
    $index += 1;
}
close $ofh;

## Read in the orf data
my %kos = ();
my %nameof = ();
open my $pfh, '<', 'putorf_cl.tsv' or die $!;
while(<$pfh>){
    chomp;
    my($contig, $start, $end, $spec, $k, $orf) = split(/\t/, $_);
    push @{$kos{$k}{$orf}}, $sp{$spec};
    my $ind = scalar(@{$kos{$k}{$orf}}) - 1;
    $nameof{$orf}{$ind} = join('_', $contig, $start, $end);
}
close $pfh;

## Make the mock-fasta
open my $cfo, '>', 'corenrps.faa' or die $!;
my %adom = ();
foreach my $k (sort keys %kos){
    my $clusterstring = '';
    my $orfnum = 1;
    my $clusta = 1;
    foreach my $o (sort keys %{$kos{$k}}){
	my $orfa = 1;
	my @r = split(/_/, $o);
	my $short = join('_', @r[0..$#r-1]);
	if($o =~ m/fwd$/){
	    my $p = 0;
	    foreach my $dom (@{$kos{$k}{$o}}){
		#$clusterstring .= $orfnum.$dom; ## add orf number
		$clusterstring .= $dom; ## AA sequence only
		$adom{$nameof{$o}{$p}} = {
		    'orfdir' => $r[-1],
		    'orf' => $short,
		    'orfdom' => $orfa,
		    'clustdom' => $clusta,
		    'spec' => $dom,
		    'clust' => $k,
		    'orfnum' => $orfnum,
		};
		$orfa += 1;
		$clusta += 1;
		$p += 1;
	    }
	}else{
	    my $p = scalar(@{$kos{$k}{$o}}) - 1;
	    foreach my $dom (reverse @{$kos{$k}{$o}}){
		#$clusterstring .= $orfnum.$dom; ## add orf number
		$clusterstring .= $dom; ## AA sequence only
		$adom{$nameof{$o}{$p}} = {
		    'orfdir' => $r[-1],
		    'orf' => $short,
		    'orfdom' => $orfa,
		    'clustdom' => $clusta,
		    'spec' => $dom,
		    'clust' => $k,
		    'orfnum' => $orfnum
		};
		$orfa += 1;
		$clusta += 1;
		$p -= 1;
	    }
	}
	$orfnum += 1;
    }
    print $cfo '>'."$k\n$clusterstring\n";
}
close $cfo;

## Make the identity matrix
my $done = 0;
my @ord = sort keys %adom;
my $torun = 0;
for my $k (1..scalar(@ord)-1){
    $torun += scalar(@ord)-$k;
}
#print STDERR "$torun pid to lookup\n";
open my $mfh, '>', 'seqsim_matrix.txt' or die $!;
foreach my $row (@ord){
    print $mfh join('|',
	       $adom{$row}{'clust'},
	       $adom{$row}{'clust'},
	       $adom{$row}{'orf'},
	       $row,
	       $adom{$row}{'spec'},
	       'A'.$adom{$row}{'clustdom'}
	);
    my $matrix = '';
    my $kill = 0;
    foreach my $col (@ord){
	$kill = 1 if($row eq $col);
	if($kill == 0){
	    my $f = 0;
	    if(exists $fid{$row}{$col}){
		$f = $fid{$row}{$col};
	    }elsif(exists $fid{$col}{$row}){
		$f = $fid{$col}{$row};
	    }else{
		#die "COMPARISON NOT FOUND:\n$row\n$col\n";
	    }
	    $matrix .= ','.$f;
	    $done += 1;
	    #print STDERR '.' if($done % 10000 == 0);
	}else{
	    last;
	}
    }
    print $mfh "$matrix\n";
}
#print STDERR "\n";
close $mfh;
