#!/bin/env perl

use strict;
use warnings;

while(<>){
    chomp;
    my($h, $s) = split(/\t/, $_);
    print '>'."$h\n$s\n";
}
